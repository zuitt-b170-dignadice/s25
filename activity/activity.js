/* 

Activity:
1. Create an activity.js file on where to write and save the solution for the activity.
2. Use the count operator to count the total number of fruits on sale.
3. Use the count operator to count the total number of fruits with stock more than 20.
4. Use the average operator to get the average price of fruits onSale per supplier.
5. Use the max operator to get the highest price of a fruit per supplier.
6. Use the min operator to get the lowest price of a fruit per supplier.
7. Create a git repository named S25.
8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
9. Add the link in Boodle.

count
count > 20 
average 
max 
min 


*/

// count 
db.fruits.aggregate(
    [
        {$match : {onSale : true}},
        {$count : "Total number of fruits on sale"}
    ]
)

// count > 20 
db.fruits.aggregate(
    [
        {$match : {stock : { $gt : 20}}},
        {$count : "Total number of fruits on with stock more than 20"}
    ]
)

// average, on Sale per supplier

/* 
    db.sales.aggregate(
   [
     {
       $group:
         {
           _id: "$item",
           avgAmount: { $avg: { $multiply: [ "$price", "$quantity" ] } },
           avgQuantity: { $avg: "$quantity" }
         }
     }
   ]
)
*/


db.fruits.aggregate([
      
    {$match : {onSale : true}},
    {
        $group : {
            _id : "$supplier_id", avgSale : { $avg : "$price"  }
        }
    }

])


// Use the max operator to get the highest price of a fruit per supplier. $max

db.fruits.aggregate(
    [
        {
            $group : {
                _id : "$supplier_id", highestPrice : { $max : "$price"  }
            }
        }
    ]
)

// Use the min operator to get the lowest price of a fruit per supplier.


db.fruits.aggregate(
    [
        {
            $group : {
                _id : "$supplier_id", highestPrice : { $min : "$price"  }
            }
        }
    ]
)



