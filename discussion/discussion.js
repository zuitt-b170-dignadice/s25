/* 
    Minit activity

    Add the the following documents for the "fruits" collection (follow the format that you have made last session)
*/


db.fruits.insertMany(
    [

        {
            f_name : "apple",
            color : "red",
            stock : 3,
            price : 20,
            supplier__id : 1234,
            onSale : true,
            origin : ["Philippines", "China"]
        },
        {
            f_name : "mango",
            color : "yellow",
            stock : 3,
            price : 20,
            supplier__id : 1234,
            onSale : false,
            origin : ["Philippines", "US"]
        },
        {
            f_name : "banana",
            color : "green ",
            stock : 20,
            price : 24,
            supplier__id : 1234,
            onSale : true,
            origin : ["Philippines", "India"]
        },
        {
            f_name : "Orange",
            color : "Orange",
            stock : 34,
            price : 15,
            supplier__id : 1234,
            onSale : true,
            origin : ["Korea", "China"]
        }
    ]

)


// find on sale


db.fruits.find(
    {
        onSale : true
    }
)

// aggregation


db.fruits.aggregate(
    [
        {
            $match : {
                onSale : true
            }
        }
    ]
)


// update your documents and make only two suppliers

/* 
update your documents and make only two supplier id : supplier id 1 and 2
*/

db.fruits.updateMany(
    {
        supplier__id: {$exists : true}
    },
    {
        set : {
            supplier__id : "1.0"
        }
    }
)


db.fruits.aggregate([
    {$match: { onSale:true}}
   ])

db.fruits.aggregate([
    {$match: { onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}}
   ])

   /* 
     "_id" : 2.0,
    "total" : 15.0
   */


/* 
   $match 
   $group 
   $project 
*/

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}}, // pipeline
    {$project: {_id: 0}}
   ]) // "total" : 45.0, remove the _id 

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}}, // pipeline
    {$project: {_id: 0, total: 1}} // 0 is to hide, 1 is to show
   ]) // "total" : 45.0, remove the _id 

// aggregate functions as a filter
// code will run from top to bottom


db.orders.aggregate( [
    // Stage 1: Filter pizza order documents by pizza size
    {
       $match: { size: "medium" }
    },
    // Stage 2: Group remaining documents by pizza name and calculate total quantity
    {
       $group: { _id: "$name", totalQuantity: { $sum: "$quantity" } }
    }
 ] )


// $sort 
db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}}, // pipeline
    {$sort: {total: 1}} // ascending 
   ]) 
   
db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}}, // pipeline
    {$sort: {total: -1}} // descending 
   ]) 

// $unwind 
db.fruits.aggregate([
    {$unwind : "$origin"}
   ]) 

   // Deconstructs an array field from the input documents to output a document for each element. Each output document is the input document with the value of the array field replaced by the element.

   // removes sub documents and creates separate document.
   
db.fruits.aggregate([
    {$unwind : "$origin"}, // $  + origin : [ountry]
    {$group : {_id : "$origin", kinds : {$sum : 1}}} // 
   ])    

// result 
 /* 1 */
(
    {
        "_id" : "India",
        "kinds" : 1.0
    },
    
    /* 2 */
    {
        "_id" : "US",
        "kinds" : 2.0 
    },
    
    /* 3 */
    {
        "_id" : "China",
        "kinds" : 1.0
    },
    
    /* 4 */
    {
        "_id" : "Ecuador",
        "kinds" : 1.0
    },
    
    /* 5 */
    {
        "_id" : "Philippines",
        "kinds" : 3.0
    }  
)

db.fruits.aggregate([
    {$unwind : "$origin"}, // $  + origin : [ountry]
    {$match : {origin : "China"}},
    {$group : {_id : "$origin", kinds : {$sum : 1}}} // 
   ])    
// result : 
(
    /* 1 */
{
    "_id" : "China",
    "kinds" : 1.0
}
)

// var owner 
var owner = ObjectId()

// create owners collection with the folllowing fields

/* 
    _id : owner,
    name : 
    contact : 
*/


db.owner.insertOne(
    {
        _id : owner,
        name : "Jherson",
        contact : "090213423"
    }
)

// create owner collection 

var owner = ObjectId()

db.owners.insert({
        _id : owner,
        name : "John Smith",
        contact : "12312412312"
    })

db.suppliers.insert(
    {
        name_sup : "ABC Fruits",
        contact : "123094231",
        owner_id : ObjectId("625573f1c646383a97a6668a")
    }
)


// multiple relationships with suppler

db.suppliers.insert(
    {
        name_sup : "DEF Fruits",
        contact : "109123415",
        address : [
            {
                street: "123 San Jose St.", 
                city : "Manila"
            },
            {
                street: "367 Gil Puyat St.", 
                city : "Makati"
            }
        ]
    }
)

// one to many relationship 


var supplier = ObjectId()
var branch1 = ObjectId()
var branch2 = ObjectId()

db.suppliers.insert(
    {
        _id : supplier,
        name : "GHI Fruits",
        contact : "01239141231",
        branches : [ branch1, branch2]
    }
)

db.branches.insertMany(
    [
        {
            _id : ObjectId("625579d1c646383a97a6668e"),
            name_b : "BG Homes",
            address : "123 Arcadio Santos St.",
            city : "Paranaque",
            supplier__id : ObjectId("625579d1c646383a97a6668d") 
        },
        {
            _id : ObjectId("625579d1c646383a97a6668f"), 
            name_b : "Rizal",
            address : "123 San Jose St.",
            city : "Manila",
            supplier__id : ObjectId("625579d1c646383a97a6668d") 
        }
    ]
)

/* 
    normalized data
        ObjectId("625579d1c646383a97a6668d") 
    
    and 
    
    denormalized data
        using reference from one objectId to another 

*/

/* 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


*/





